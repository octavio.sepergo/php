<?php

use Illuminate\Support\Facades\Route;
use App\Viaje;
use App\Destino;
use App\Plan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', function () {
    return view('login');
});
Route::post('login', 'inicio@login')->name('login');

Route::post('VotarD', 'inicio@VotarD')->name('VotarD');
Route::post('QVotarD', 'inicio@QVotarD')->name('QVotarD');
Route::post('VotarP', 'inicio@VotarP')->name('VotarP');
Route::post('QVotarP', 'inicio@QVotarP')->name('QVotarP');

Route::get('logout', 'inicio@logout')->name('logout');

Route::post('buscarViaje', 'inicio@buscarViaje')->name('buscarViaje');


Route::post('newViaje', 'inicio@newViaje')->name('newViaje');



Route::post('ClonViaje', 'inicio@ClonViaje')->name('ClonViaje');

Route::get('principal', 'inicio@Viajes' )->name('principal');

Route::post('viaje', 'inicio@oneViaje')->name('viaje');

Route::get('viaje/{id}', function ($id) {
    $viaje = Viaje::find($id);
    if($viaje == null){
        return redirect()->route('principal');
    }
    $destinos =Viaje::find($id)->Destino;
    $planes = Viaje::find($id)->Plan;
    $actividades = Viaje::find($id)->RegistroActividad->reverse();
    $viajeros = Viaje::find($id)->Viajeros;
    $colaboradores = Viaje::find($id)->Colaboradores;
    $participantes = Viaje::find($id)->Viajeros;
    $participantes = $participantes->merge($colaboradores);
    $opiniones = Viaje::find($id)->Calificar;
    $number = 0;
    return view('Viajes/oneviaje', compact('viaje','planes','actividades','participantes','destinos','viajeros','colaboradores','opiniones', 'number'));
})->name('viaje2');

Route::get('MisViajes', 'inicio@MisViajes' )->name('MisViajes')->Middleware('logeadoMidd');;

Route::post('finViaje', 'inicio@finViaje')->name('finViaje');

Route::post('registro', 'inicio@registro')->name('registro');

Route::view('welcome_message', 'welcome_message')->name('welcome_message');

Route::post('newOpinion', 'inicio@newOpinion')->name('newOpinion');

Route::resource('/', 'Welcome');

Route::post('newPlan', 'inicio@newPlan')->name('newPlan');

Route::post('newDestino', 'inicio@newDestino')->name('newDestino');

Route::post('newViajero', 'inicio@newViajero')->name('newViajero');

Route::post('newColab', 'inicio@newColab')->name('newColab');

Route::post('registroInvitado', 'inicio@registroInvitado')->name('registroInvitado');

Route::post('aceptarInvit', 'inicio@aceptarInvit')->name('aceptarInvit');
Route::post('aceptarInvit2', 'inicio@aceptarInvit2')->name('aceptarInvit2');
Route::get('aceptarInvit', 'inicio@aceptarInvit')->name('aceptarInvit')->middleware('invitacionMidd');;
Route::get('aceptarInvit2', 'inicio@aceptarInvit2')->name('aceptarInvit2')->middleware('invitacionMidd');;

