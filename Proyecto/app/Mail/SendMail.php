<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $colab;
    public function __construct($data,$colab)
    {
        $this->data = $data;
        $this->colab = $colab;
    }


    public function build()
    {
        if($this->colab==true){
            return $this->from('proyectophp01@gmail.com')->subject('New Customer Equiry')->view('Plantilla/mensaje')->with('data',$this->data);
        }else{
            return $this->from('proyectophp01@gmail.com')->subject('New Customer Equiry')->view('Plantilla/mensaje2')->with('data',$this->data);
        }

    }
}