<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    // Declaración de una propiedad
    protected $table = 'usuario';
    public $incrementing = false;
    protected $primaryKey = 'nickname';
    protected $keyType = 'string';
    public $timestamps = false;

    public function Viaje()
    {
        return $this->belongsToMany('App\Viaje', 'usuario_viaje', 'nickname', 'idviaje');
    }

}
?>