<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Destino extends Model
{
    // Declaración de una propiedad
    protected $table = 'destino';
    protected $primaryKey = 'iddestino';
    public $timestamps = false;

    public function Voto()
    {
        return $this->hasMany('App\Voto','idDestino');
    }

}
?>