<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Viaje extends Model{

    protected $table = 'viaje';
    protected $primaryKey = 'idviaje';
    public $timestamps = false;

    public function Usuario()
    {
        return $this->belongsToMany('App\Usuario','usuario_viaje', 'idviaje','nickname' );
    }

    public function Viajeros()
    {
        return $this->belongsToMany('App\Usuario','viajeros_viaje', 'idviaje','nickname'); 
    }
    public function Colaboradores()
    {
        return $this->belongsToMany('App\Usuario','cola_viaje', 'idviaje','nickname');
    }    

    public function Plan()
    {
        return $this->hasMany('App\Plan','Viaje_plan','idviaje','idplan');
    }
    public function Destino()
    {
        return $this->hasMany('App\Destino','Viaje_destino','idviaje','iddestino');
    }

    public function RegistroActividad()
    {
        return $this->hasMany('App\RegistroActividad','Viajes_registroActividad','idviaje','idRegistroActividad');
    }

    public function Calificar()
    {
        return $this->belongsToMany('App\Calificar','viaje_calificar','idviaje','idcalificar');
    }




}
