<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    // Declaración de una propiedad
    protected $table = 'plan';
    protected $primaryKey = 'idplan';
    public $timestamps = false;

    public function Voto()
    {
        return $this->hasMany('App\Voto','idPlan');
    }

}
?>