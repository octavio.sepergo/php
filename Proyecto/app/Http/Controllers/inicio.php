<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\User;
use Illuminate\Http\Request;
use App\Usuario;
use App\Viaje;
use App\Destino;
use App\Plan;
use App\Calificar;
use App\Voto;
use App\RegistroActividad;
use Intervention\Image\Size;

class inicio extends Controller
{
    public function registro(Request $request)
    {

 
        $Nick = $request->get('Nick');
        $nombre = $request->get('nombre');
        $telefono = $request->get('telefono');
        $email = $request->get('email');
        $pass = $request->get('pass');
        $biografia = $request->get('biografia');    
        

        $img = $request->file('img');
        $img2 = $Nick.$img->getClientOriginalName();
        $img->move(public_path().'/img/', $img2) ;


        $us = new Usuario;

        $us->nickname = $Nick;
        $us->email = $email;
        $us->telefono = $telefono;
        $us->NombreCompleto  = $nombre;
        $us->imagen = $img2;
        $us->pass =$pass;
        $us->biografia =$biografia;

        $us->save();

        $request->session()->put('nick', $Nick);

        return redirect()->route('principal');

    }
    public function registroInvitado(Request $request)
    {

 
        $Nick = $request->get('Nick');
        $nombre = $request->get('nombre');
        $telefono = $request->get('telefono');
        $email = $request->get('email');
        $pass = $request->get('pass');
        $biografia = $request->get('biografia');    
        

        $img = $request->file('img');
        $img2 = $Nick.$img->getClientOriginalName();
        $img->move(public_path().'/img/', $img2) ;


        $us = new Usuario;

        $us->nickname = $Nick;
        $us->email = $email;
        $us->telefono = $telefono;
        $us->NombreCompleto  = $nombre;
        $us->imagen = $img2;
        $us->pass =$pass;
        $us->biografia =$biografia;

        $us->save();

        $id = $request->get('id');
        $viaje = Viaje::find($id);
        $tipo = $request->get('tipo');
        if($tipo == "col"){
            $this->addRegistroActividad($Nick,"aceptarInvit","Se sumo como Colaborador",$id);
            $us->viaje()->save($viaje);
           $viaje->Colaboradores()->save($us);  
        }
       else{
            $this->addRegistroActividad($Nick,"aceptarInvit2","Se sumo como Viajero",$id);
            $us->viaje()->save($viaje);
            $viaje->Viajeros()->save($us);
       }
        
        $request->session()->put('nick', $Nick);

        return redirect()->route('viaje2', ['id' => $id]);
        

    }

    public function newViaje(Request $request)
    {

 
        $nombre = $request->get('nombreV');
        $descripcion = $request->get('descripcion');
        $privacidad = $request->get('privacidad');
        $categoria = $request->get('categoria');

        $Nick = $request->session()->get('nick');
        $dat = Usuario::find($Nick);

        $viaje = new Viaje();

        $viaje->nombre = $nombre;
        $viaje->descripcion = $descripcion;
        $viaje->privado = true;
        $viaje->ejecutado = false;
        $viaje->categoria = $categoria;
        $viaje->nick = $Nick;

        $viaje->save();
        $dat->viaje()->save($viaje);
        $viaje->Viajeros()->save($dat);

        return redirect()->route('principal');
    }
    public function ClonViaje(Request $request)
    {

 
        $nombre = $request->get('nombreV');
        $descripcion = $request->get('descripcion');
        $privacidad = $request->get('privacidad');
        $categoria = $request->get('categoria');
        $id = $request->get('id');
        $dat2 = Viaje::find($id)->Plan;
        $dat3 = Viaje::find($id)->Destino;



        $Nick = $request->session()->get('nick');
        $dat = Usuario::find($Nick);

        $viaje = new Viaje();

        $viaje->nombre = $nombre;
        $viaje->descripcion = $descripcion;
        $viaje->privado = true;
        $viaje->ejecutado = false;
        $viaje->categoria = $categoria;
        $viaje->nick = $Nick;    
        $viaje->save();

          foreach($dat2 as $item){

           $plan = new Plan();
           $plan->nombre = $item->nombre;
           $plan->descripcion = $item->descripcion;
           $plan->ubicacion = $item->ubicacion;
           $plan->link = $item->link;
           $plan->usuario = $item->usuario;
   
           $plan->save();
           $viaje->Plan()->save($plan);

        }
        foreach($dat3 as $item){
 
            $destino = new Destino();

            $destino->pais =  $item->pais;
            $destino->ciudad = $item->ciudad;
            $destino->sitio = $item->sitio;
            $destino->usuario = $item->usuario;
    
            $viaje->Destino()->save($destino);
         }
         $dat->viaje()->save($viaje);
         $viaje->Viajeros()->save($dat);

        return redirect()->route('viaje2' ,['id' => $viaje->idviaje]);

    }
    

    public function login(Request $request)
    {

        $Nick = $request->get('Nick');
        $pass = $request->get('pass');
            
        $dat = Usuario::find($Nick);
        if($dat == null ){
            return redirect()->route('login');
        }
        if($dat->pass == $pass){

             $request->session()->put('nick', $Nick);
             return redirect()->route('principal');
        }
        else{
            return redirect()->route('login'); 
        }

       
    }
    public function logout(Request $request)
    {
        $request->session()->forget('nick');
        return redirect()->route('principal');

    }

    public static function img($Nick)
    {
        $dat = Usuario::find($Nick);
        return $dat->imagen;
    }

    public function buscarViaje(Request $request)
    {
        $viaje = $request->get('viaje');
        $var2 = str_replace("#","",$viaje);
        $viajes = Viaje::where('categoria','=', $var2)->where('privado','=', 0)->where('ejecutado','=', 1)->get();
        $actividades = RegistroActividad::where('privado', 0)->get();
        return view('Viajes/allviajes', compact('viajes','actividades'));
    }

    public function Viajes()
    {
        $viajes = Viaje::where('privado','=', 0)->where('ejecutado','=', 1)->get();
        $actividades = RegistroActividad::where('privado', 0)->get();
        return view('Viajes/allviajes', compact('viajes','actividades'));
    }

    public function MisViajes(Request $request)
    {
        $Nick = $request->session()->get('nick');
        $viajes = Usuario::find($Nick)->Viaje;
        $actividades = RegistroActividad::where('privado', 0)->get();
        return view('Viajes/allviajes', compact('viajes','actividades'));
    }



    public static function oneViaje(Request $request)
    {
        $id = $request->get('id');
        $viaje = Viaje::find($id);
        $planes = Viaje::find($id)->Plan;
        $actividades = Viaje::find($id)->RegistroActividad->reverse();
        $destinos =Viaje::find($id)->Destino;
        $viajeros = Viaje::find($id)->Viajeros;
        $colaboradores = Viaje::find($id)->Colaboradores;
        $participantes = Viaje::find($id)->Viajeros;
        $participantes = $participantes->merge($colaboradores);
        $opiniones = Viaje::find($id)->Calificar;
        $number = 0;
        return view('Viajes/oneviaje', compact('viaje','planes','actividades','participantes','destinos','viajeros','colaboradores','opiniones', 'number'));

    }

    public function newPlan(Request $request)
    {

        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $nombre = $request->get('nombre');
        $descripcion = $request->get('descripcion');
        $ubicacion = $request->get('ubicacion');
        $link = $request->get('link');
        $id = $request->get('id');
        $dat = Viaje::find($id);
        $Nick = $request->session()->get('nick');
        $plan = new Plan();
        $plan->lat = $lat;
        $plan->lng = $lng;
        $plan->nombre = $nombre;
        $plan->descripcion = $descripcion;
        $plan->ubicacion = $ubicacion;
        $plan->link = $link;
        $plan->aprovado = false;
        $plan->usuario = session()->get('nick');

        $plan->save();
        $dat->Plan()->save($plan);
        $this->addRegistroActividad(session()->get('nick'),"Nuevo Plan"," Agrego un nuevo plan",$id);
        return redirect()->route('viaje2', ['id' => $id]);
    }
    public function newDestino(Request $request)
    {
        $pais = $request->get('pais');
        $ciudad = $request->get('ciudad');
        $sitio = $request->get('sitio');
        $Nick = $request->session()->get('nick');
        $id = $request->get('id');
        $viaje = Viaje::find($id);

        $destino = new Destino();
        $destino->pais = $pais;
        $destino->ciudad = $ciudad;
        $destino->sitio = $sitio;
        $destino->usuario = $Nick;
        $destino->aprovado = false;
        $destino->save();

        $viaje->Destino()->save($destino);
        $this->addRegistroActividad(session()->get('nick'),"Nuevo Destino"," Agrego un nuevo destino",$id);
        return redirect()->route('viaje2' ,['id' => $id]);
    }

    public function addRegistroActividad($nick,$tipo,$actividad,$id)
    {

        $dat = Viaje::find($id);
        $Actividad = new RegistroActividad();
        $Actividad->usuario = $nick;
        $Actividad->privado = $dat->privado;
        $Actividad->idViaje = $id;
        $Actividad->actividad = $actividad;
        $Actividad->nombreViaje = $dat->nombre;
        $Actividad->tipo = $tipo;
        $Actividad->save();
        $dat->RegistroActividad()->save($Actividad);

    }   
    public function verRegistroActividad($idplan)
    {

        $dat = Viaje::find($idplan);
        $Actividad = Viaje::find($idplan)->RegistroActividad;
        return $Actividad;
               
    }

    public function newViajero(Request $request)
    {
        $id = $request->get('id');
        $colab = false;
        $data = array(
            'id' => $request->get('id'),
            'email' => $request->get('emailviajero'),
            'name' => $request->session()->get('nick'),
            'message' => 'Quieres unirte como un viajero mas??'
        );
        Mail::to($request->get('emailviajero'))->send(new SendMail($data,$colab));
        $this->addRegistroActividad(session()->get('nick'),"newViajero"," Invito a un nuevo viajero",$id);
        return redirect()->route('viaje2', ['id' => $id]);
    }
    public function newColab(Request $request)
    {
        $id = $request->get('id');
        $colab = true;
        $data = array(
            'id' => $request->get('id'),
            'email' => $request->get('emailcolab'),
            'name' => $request->session()->get('nick'),
            'message' => 'Quieres unirte como colaborador??'
        );
        Mail::to($request->get('emailcolab'))->send(new SendMail($data,$colab));
        $this->addRegistroActividad(session()->get('nick'),"newColab"," Invito a un nuevo Colaborador",$id);
        return redirect()->route('viaje2', ['id' => $id]);
    }
    public function aceptarInvit(Request $request)
    {
        $id = $request->get('id');
        $email = $request->get('email');
        $invi = Usuario::where('email',"=", $email)->first();
        $viaje = Viaje::find($id);
        $tipo = "col";
        if($invi == null){
            return view('welcome', compact('id','email','tipo'));
        }
        else{
            $invi->viaje()->save($viaje);
            $viaje->Colaboradores()->save($invi);
            $this->addRegistroActividad($invi->nickname,"aceptarInvit","Se sumo como Colaborador",$id);
            return redirect()->route('viaje2', ['id' => $id]);
        }

       
    }
    public function aceptarInvit2(Request $request)
    {
        $id = $request->get('id');
        $email = $request->get('email');
        $invi = Usuario::where('email',"=", $email)->first();
        $viaje = Viaje::find($id);
        $tipo = "via";
        if($invi == null){
            return view('welcome', compact('id','email','tipo'));
        }
        else{
            $invi->viaje()->save($viaje);
            $viaje->Viajeros()->save($invi);
            $this->addRegistroActividad($invi->nickname,"aceptarInvit2","Se sumo como Viajero",$id);
            return redirect()->route('viaje2', ['id' => $id]); 
        }
       
    }
    public static  function VotarD(Request $request)
    {
        $idD = $request->get('iddestino');
        $nick = $request->get('nick');
        $idV = $request->get('idviaje');
        $destino = Destino::find($idD);
        $voto = new Voto();
        $voto->usuario = $nick;
        $voto->save();

        
        $destino->Voto()->save($voto);
        $viajeros = Viaje::find($idV)->Viajeros;
        $votos = Destino::find($idD)->Voto;
        $destino = Destino::find($idD);
        if(sizeof($viajeros) == sizeof($votos)){
            $destino->aprovado = true;
            $destino->save();
        }
        return redirect()->route('viaje2', ['id' => $idV]); 
    }
    public static  function isVotoD($idD,$nick)
    {
        
        $votos = Destino::find($idD)->Voto;
        $ret = false;
        foreach($votos as $v){
            if($v->usuario==$nick){
                $ret = true;
            }

        }
       
        return $ret;
    }
    public static  function isVotoP($idD,$nick)
    {

        $votos = Plan::find($idD)->Voto;
        $ret = false;
        foreach($votos as $v){
            if($v->usuario==$nick){
                $ret = true;
            }

        }
       
        return $ret;
    }

    public static  function QVotarD(Request $request)
    {
        $idD = $request->get('iddestino');
        $nick = $request->get('nick');
        $idV = $request->get('idviaje');
        $votos = Destino::find($idD)->Voto;

        foreach($votos as $v){
            if($v->usuario==$nick){
                $v->idDestino = null;
                $v->save();
            }

        }
        return redirect()->route('viaje2', ['id' => $idV]); 

    }

    public static  function VotarP(Request $request)
    {
        $idD = $request->get('iddestino');
        $nick = $request->get('nick');
        $idV = $request->get('idviaje');
        $plan = Plan::find($idD);
        $voto = new Voto();
        $voto->usuario = $nick;
        $voto->save();

        
        $plan->Voto()->save($voto);
        $viajeros = Viaje::find($idV)->Viajeros;
        $votos = Plan::find($idD)->Voto;
        $plan = Plan::find($idD);
        if(sizeof($viajeros) == sizeof($votos)){
            $plan->aprovado = true;
            $plan->save();
        }
        return redirect()->route('viaje2', ['id' => $idV]); 
    }

    public static  function QVotarP(Request $request)
    {
        $idD = $request->get('iddestino');
        $nick = $request->get('nick');
        $idV = $request->get('idviaje');
        $votos = Plan::find($idD)->Voto;

        foreach($votos as $v){
            if($v->usuario==$nick){
                $v->idPlan = null;
                $v->save();
            }

        }
        return redirect()->route('viaje2', ['id' => $idV]); 

    }

    public function finViaje(Request $request)
    {
        $id = $request->get('id');
        $dat = Viaje::find($id);
        $dat->ejecutado = 1;
        $dat->save();
        return redirect()->route('viaje2', ['id' => $id]);
    }

    public function newOpinion(Request $request)
    {
        $id = $request->get('id');
        $dat = Viaje::find($id);
        $calificar = new Calificar();

        $calificar->usuario = session()->get('nick');
        if(session()->get('nick') == ''){
            return redirect()->route('viaje2', ['id' => $id]);
        }else{
            $coment = $request->get('coment');
            if($coment == ''){
                $calificar->texto = 'Sin comentarios';
            }else{
                $calificar->texto = $coment;
            }

            $punt = $request->get('rating');
            $calificar->puntaje = $punt;
            $calificar->save();

            $dat->Calificar()->save($calificar);

            return redirect()->route('viaje2', ['id' => $id]);
        }

    }

    public static  function isCalificado($idV,$nick)
    {

        $Calificar = Viaje::find($idV)->Calificar;
        $ret = false;
        foreach($Calificar as $v){
            if($v->usuario==$nick){
                $ret = true;
            }

        }
       
        return $ret;
    }


}
