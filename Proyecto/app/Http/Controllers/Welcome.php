<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Welcome extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function index()
	{
		$data['titulo'] = 'Bienvenido a TripDo';
		
	
		return view('welcome_message');

	}
}
