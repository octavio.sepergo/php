<?php

namespace App\Http\Middleware;

use Closure;

class invitacionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->has('emailviajero') && !$request->has('emailcolab')){
        return redirect()->route('principal');
        }
       
        return $next($request);
    }
}
