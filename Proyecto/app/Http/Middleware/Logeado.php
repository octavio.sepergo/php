<?php

namespace App\Http\Middleware;

use Closure;

class Logeado
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {  
        if(session()->get('nick') == null){
            return redirect()->route('principal');
            //return route('welcome_message');
        }
        return $next($request);
    }
}
