<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;

use Closure;
use App\Viaje;


class viajeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $viaje = Viaje::where('idviaje','=', 1)->get();
        if ($viaje  === null) {
        return redirect()->route('principal');
       }
        return $next($request);
    }
}
