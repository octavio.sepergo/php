@extends('Plantilla/plantilla')

@section('contenido')

@php($con = 0)
@if($viaje->privado)
@foreach($participantes as $participantesItem)
				
				@if($participantesItem->nickname == session()->get('nick'))	
				@php($con = 1)
				@endif

@endforeach
@if($con == 0){
	<script>window.location = "/principal";</script>
}
@endif
@endif
<div class="conteiner" id="contenido">
	<div class="card" id="viajes">
		<div class="card text-center" style="border: none;">
			<div class="card-body" style="display: flex;justify-content: space-between;">
				<a href="#"><i class="fas fa-long-arrow-alt-left" style="font-size: 30px;"></i></a>
				@if(session()->get('nick') != null)
				<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal2">Clonar Viaje</button>
				@endif
			</div>

			<div style="height: 150px;height: 100px;display: flex;flex-direction: column;justify-content: space-between;">

				<div style="display: flex;flex-direction: column;justify-content: space-between;">

					<div>
						<h5 class="card-title" style=" font-size: 30px;font-weight: 600;text-align: start;"> {{ $viaje->nombre }}</h5>
					</div>
					<div style="text-align: initial;display: flex;flex-direction: row;    padding-left: 20px;">
						@if($colaboradores->isEmpty())
						@else
						@foreach($colaboradores as $colabo)
							@if($number >= 3)
								@break
							@else
							<div class="card" style="border:none;margin-left: -10px;    border-radius: 20px;border: 5px solid white;">
								<img class="card-img-top"
									src="/img/<?=App\Http\Controllers\inicio::img($colabo->nickname)?>"
									alt="Card image cap"
									style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">
							</div>
							@php($number++)
							@endif
						@endforeach
						@endif
						@if($viajeros->isEmpty())
						@else
						@foreach($viajeros as $viajeroo)
						@if($number >= 3)
								@break
							@else	
							<div class="card" style="border:none;margin-left: -10px;    border-radius: 20px;border: 5px solid white;">
									<img class="card-img-top"
										src="/img/<?=App\Http\Controllers\inicio::img($viajeroo->nickname)?>"
										alt="Card image cap"
										style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">
							</div>
							@php($number++)
							@endif	
						@endforeach
						@endif

						<div id="fb-root"></div>
						<script>(function(d, s, id) {
							var meta = document.createElement('meta');
							meta.property= "og:image";
							meta.content="https://2.bp.blogspot.com/--3LOaU0HSNc/T3yLIRpWNTI/AAAAAAAALVo/o9XLrdsw4nY/s1600/lugares+hd+%282%29.jpg";
							document.getElementsByTagName('head')[0].appendChild(meta);
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id)) return;
							js = d.createElement(s); js.id = id;
							js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						

						<div class="fb-share-button" 
							data-size="large"
							data-href="http://127.0.0.1:8000/viaje/{{$viaje->idviaje}}" 
							data-layout="button">
						</div>
				@if($viaje->ejecutado == 0)

						@foreach($viajeros as $viajerosItem)
							@if($viajerosItem->nickname == session()->get('nick'))	
							<div style="    display: flex;justify-content: flex-end;    padding: 10px;">
								<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#invitar">INVITAR</button>
							</div>
							@endif
						@endforeach 
						
					@if($viaje->nick == session()->get('nick'))
                        <div class="card-body" style="    display: flex;justify-content: flex-end;    padding: 10px;">
                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#finalizar"
                                    style="margin-right: 10px;">Finalizar viaje</button>
						</div>
						@endif
						
                    @else
                    <div class="card-body" style="    display: flex;justify-content: flex-end;    padding: 10px;">
                            <p>Viaje finalizado</p>
                    </div>
				@endif	
				
				
                </div>
			</div>
		</div>	
		<div class="card" style="border: none">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
						aria-controls="profile" aria-selected="false">Destinos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
						aria-controls="contact" aria-selected="false">Planes</a>
				</li>
				@if($viaje->ejecutado == 0)
				@foreach($participantes as $participantesItem)
				@if($participantesItem->nickname == session()->get('nick'))	
				<li class="nav-item">
					<a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab"
						aria-controls="profile2" aria-selected="false">Destinos Sugeridos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab"
						aria-controls="contact2" aria-selected="false">Planes Sugeridos</a>
				</li>
				@endif
				@endforeach
				@endif
						@if($viaje->ejecutado == 1)
						<li class="nav-item">
							<a class="nav-link" id="comentario-tab" data-toggle="tab" href="#comentario" role="tab"
								aria-controls="comentario" aria-selected="false">Opinion</a>
						</li>
						@endif
	
			</ul>

			<div class="tab-content" id="myTabContent">

				<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
				@foreach($participantes as $participantesItem)
					@if($participantesItem->nickname == session()->get('nick') && $viaje->ejecutado == 0)	
					<div class="card-body" style="display: flex;justify-content: flex-end;    padding: 10px;">
						<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#adddestino"
							style="margin-right: 10px;">Agregar</button>
					</div>
					@endif
				@endforeach
				@if($destinos->isEmpty())
						<div>
							<div class="plan" style="padding: 20px;">
								<h5 class="card-title" >No hay Destinos para este viaje aun.</h5>
							</div>
						</div>
					@else
						@foreach ($destinos as $destino)
						@if($destino->aprovado)
						<div class="destino" style="padding: 20px;">
							<div style="margin: 10px;">
							<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-right: 10px">PAIS</small>
								<h2 id="lugar" class="form-text text-muted" style="font-size: 20px">#{{$destino->pais}}</h2>
							</div>
							<div class="destinos">
								<div class="card" style="border: none ">
									<div style="display: flex;padding: 10px">
									<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-right: 10px">DESTINO</small>
										<h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px">#{{$destino->ciudad}}</h5>
				
									</div>
									<div style="display: flex;flex-direction: row;align-items: center;padding: 10px">
										<div class="img-perfil" style="padding-right: 10px">
											<img class="card-img-top" src="/img/<?=App\Http\Controllers\inicio::img($destino->usuario)?>" alt="Card image cap" style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">
										</div>
										<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-right: 10px">Agregado por</small>
										<h7 class="card-title" style="margin-bottom: 0px">{{$destino->usuario}}</h7>
										
										<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-left: 50px">Sitios Populares:</small>
										<h7 class="card-title" style="margin-bottom: 0px;padding-left: 50px ;font-family: cursive;">{{$destino->sitio}}</h7>
									</div>
								</div>
							</div>
							<hr>
						</div>
						@endif
						@endforeach
					@endif
				</div>
				<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
				@foreach($participantes as $participantesItem)
					@if($participantesItem->nickname == session()->get('nick') && $viaje->ejecutado == 0)	
					<div class="card-body" style="    display: flex;justify-content: flex-end;    padding: 10px;">
						<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#addplan"
							style="margin-right: 10px;">Agregar</button>
					</div>
					@endif
				@endforeach	
				<div style="overflow: scroll;height: 500px;">
					@if($planes->isEmpty())
						<div>
							<div class="plan" style="padding: 20px;">
								<h5 class="card-title" >No hay Planes para este viaje aun.</h5>
							</div>
						</div>
					@else
					@foreach ($planes as $plan)
					@if($plan->aprovado)				
					<div class="plan" style="padding: 20px;">
						<div class="planes">
							<div class="card" style="border: none ">
							<div style="display: flex;align-items: center;justify-content: space-between;">
								<div style="display: flex;padding: 10px">
									<h2 class="card-title" style="margin-bottom: 0px;padding-right: 20px">{{$plan->nombre}}</h2>
									
								</div>
								<div style="display: flex;padding: 10px">
								<h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px">{{$plan->descripcion}}</h5>
								</div>
								<div style="margin: 10px;">
									<small id="lugar" class="form-text text-muted" style="font-size: 20px">#{{$plan->ubicacion}}</small>
								</div>
							</div>
								<div style="display: flex;flex-direction: row;align-items: center;padding: 10px">
									<div class="img-perfil" style="padding-right: 10px">
										<img class="card-img-top"src="/img/<?=App\Http\Controllers\inicio::img($plan->usuario)?>"alt="Card image cap"style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">
									</div>
									<small id="agregadopor" class="form-text text-muted"
										style="margin-top: 0px;padding-right: 10px">Agregado por</small>
									<h7 class="card-title" style="margin-bottom: 0px">{{$plan->usuario}}</h7>
									<h7 class="card-title" style="margin-bottom: 0px;padding-left: 50px ;font-family: cursive;">LINK :<a href="{{$plan->link}}">{{$plan->link}}</a>
									</h7>
								</div>
							</div>
						</div>
						<hr>
					</div>
					@endif
					@endforeach
					@endif
					</div>


				</div>

				<div class="tab-pane fade " id="profile2" role="tabpanel" aria-labelledby="profile2-tab">
				@foreach($participantes as $participantesItem)
					@if($participantesItem->nickname == session()->get('nick'))	
					<div class="card-body" style="display: flex;justify-content: flex-end;    padding: 10px;">
						<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#adddestino"
							style="margin-right: 10px;">Agregar</button>
					</div>
					@endif
				@endforeach
				@if($destinos->isEmpty())
						<div>
							<div class="plan" style="padding: 20px;">
								<h5 class="card-title" >No hay Destinos Sugeridos para este viaje aun.</h5>
							</div>
						</div>
					@else
						@foreach ($destinos as $destino)
						@if(!$destino->aprovado)

						<div class="destino" style="padding: 20px;">
							<div style="margin: 10px;">
							<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-right: 10px">PAIS</small>
								<h2 id="lugar" class="form-text text-muted" style="font-size: 20px">#{{$destino->pais}}</h2>
							</div>
							<div class="destinos">
								<div class="card" style="border: none ">
									<div style="display: flex;padding: 10px">
									<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-right: 10px">DESTINO</small>
										<h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px">#{{$destino->ciudad}}</h5>
				
									</div>
									<div style="display: flex;flex-direction: row;align-items: center;padding: 10px">
										<div class="img-perfil" style="padding-right: 10px">
											<img class="card-img-top" src="/img/<?=App\Http\Controllers\inicio::img($destino->usuario)?>" alt="Card image cap" style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">
										</div>
										<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-right: 10px">Agregado por</small>
										<h7 class="card-title" style="margin-bottom: 0px">{{$destino->usuario}}</h7>
										
										<small id="agregadopor" class="form-text text-muted" style="margin-top: 0px;padding-left: 50px">Sitios Populares:</small>
										<h7 class="card-title" style="margin-bottom: 0px;padding-left: 50px ;font-family: cursive;">{{$destino->sitio}}</h7>
										<div class="card-body" style="display: flex;justify-content: flex-end; ">
										@foreach($viajeros as $viajerosItem)
											@if($viajerosItem->nickname == session()->get('nick'))	
											@if(App\Http\Controllers\inicio::isVotoD($destino->iddestino,session()->get('nick')))
											<form action="{{ route('QVotarD') }}" method="POST">
											{{ csrf_field() }}
											<input id="1" name="iddestino" type="hidden" value="{{$destino->iddestino}}">
											<input id="2" name="nick" type="hidden" value="{{session()->get('nick')}}">
											<input id="3" name="idviaje" type="hidden" value="{{$viaje->idviaje}}">
												<button type="submit" class="btn btn-dark" >Quitar voto</button>
											</form>		
											@else
											<form action="{{ route('VotarD') }}" method="POST">
											{{ csrf_field() }}
											<input id="1" name="iddestino" type="hidden" value="{{$destino->iddestino}}">
											<input id="2" name="nick" type="hidden" value="{{session()->get('nick')}}">
											<input id="3" name="idviaje" type="hidden" value="{{$viaje->idviaje}}">
												<button type="submit" class="btn btn-dark" >Votar</button>
											</form>	
											@endif
											@endif
											@endforeach
										</div>
									</div>
									
								</div>
							</div>
							<hr>
						</div>
						@endif
						@endforeach
					@endif
				</div>
				<div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact2-tab">
				@foreach($participantes as $participantesItem)
					@if($participantesItem->nickname == session()->get('nick'))	
					<div class="card-body" style="    display: flex;justify-content: flex-end;    padding: 10px;">
						<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#addplan"
							style="margin-right: 10px;">Agregar</button>
					</div>
					@endif
				@endforeach	
				<div style="overflow: scroll;height: 500px;">
					@if($planes->isEmpty())
						<div>
							<div class="plan" style="padding: 20px;">
								<h5 class="card-title" >No hay Planes sugeridos para este viaje aun.</h5>
							</div>
						</div>
					@else
					@foreach ($planes as $plan)
					@if(!$plan->aprovado)
					<div class="plan" style="padding: 20px;">
						<div class="planes">
							<div class="card" style="border: none ">
							<div style="display: flex;align-items: center;justify-content: space-between;">
								<div style="display: flex;padding: 10px">
									<h2 class="card-title" style="margin-bottom: 0px;padding-right: 20px">{{$plan->nombre}}</h2>
									
								</div>
								<div style="display: flex;padding: 10px">
								<h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px">{{$plan->descripcion}}</h5>
								</div>
								<div style="margin: 10px;">
									<small id="lugar" class="form-text text-muted" style="font-size: 20px">#{{$plan->ubicacion}}</small>
								</div>
							</div>
								<div style="display: flex;flex-direction: row;align-items: center;padding: 10px">
									<div class="img-perfil" style="padding-right: 10px">
										<img class="card-img-top"src="/img/<?=App\Http\Controllers\inicio::img($plan->usuario)?>"alt="Card image cap"style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">
									</div>
									<small id="agregadopor" class="form-text text-muted"
										style="margin-top: 0px;padding-right: 10px">Agregado por</small>
									<h7 class="card-title" style="margin-bottom: 0px">{{$plan->usuario}}</h7>
									<h7 class="card-title" style="margin-bottom: 0px;padding-left: 50px ;font-family: cursive;">LINK :<a href="{{$plan->link}}">{{$plan->link}}</a>
									</h7>
									<div class="card-body" style="display: flex;justify-content: flex-end; ">
									@foreach($viajeros as $viajerosItem)
											@if($viajerosItem->nickname == session()->get('nick'))	
											@if(App\Http\Controllers\inicio::isVotoP($plan->idplan,session()->get('nick')))
											<form action="{{ route('QVotarP') }}" method="POST">
											{{ csrf_field() }}
											<input id="1" name="iddestino" type="hidden" value="{{$plan->idplan}}">
											<input id="2" name="nick" type="hidden" value="{{session()->get('nick')}}">
											<input id="3" name="idviaje" type="hidden" value="{{$viaje->idviaje}}">
												<button type="submit" class="btn btn-dark" >Quitar voto</button>
											</form>		
											@else
											<form action="{{ route('VotarP') }}" method="POST">
											{{ csrf_field() }}
											<input id="1" name="iddestino" type="hidden" value="{{$plan->idplan}}">
											<input id="2" name="nick" type="hidden" value="{{session()->get('nick')}}">
											<input id="3" name="idviaje" type="hidden" value="{{$viaje->idviaje}}">
												<button type="submit" class="btn btn-dark" >Votar</button>
											</form>	
											@endif
											@endif
											@endforeach
										</div>
								</div>
							</div>
						</div>
						<hr>
					</div>
					@endif
					@endforeach
					@endif
					</div>


				</div>
				<div class="tab-pane fade" id="comentario" role="tabpanel" aria-labelledby="comentario-tab">
					@if($viaje->ejecutado == 1)
					@foreach($viajeros as $viajerosItem)
						@if($viajerosItem->nickname == session()->get('nick') &&  ! App\Http\Controllers\inicio::isCalificado($viaje->idviaje,session()->get('nick')))	
						<div class="card-body" style="display: flex;justify-content: flex-end;    padding: 10px;">
							<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#addopinion"
								style="margin-right: 10px;">Opinar</button>
						</div>
						@endif
						@endforeach
					@endif
					<div style="overflow: scroll;height: 500px;">
					@if($opiniones->isEmpty())
						<div>
							<div class="plan" style="padding: 20px;">
								<h5 class="card-title" >No hay Opiniones para este viaje aun.</h5>
							</div>
						</div>
					@else
					<hr>
					@foreach ($opiniones as $opinion)
					<div>
						<div style="display: flex;flex-direction: row;align-items: center;padding-left: 10px;justify-content: space-between;">
						@if($opinion->texto == 'Sin comentarios')
							<p style="color: gray;">{{ $opinion->texto}}</p> 
						@else
							<p style="color: black;font-weight:600;">{{ $opinion->texto}}</p> 
						@endif
							<div>
								<div>
									<p style="padding-right: 20px;color: black;font-weight:600">Puntaje: {{ $opinion->puntaje}} estrella</p>
								</div>
								<div class="star2">

										@if($opinion->puntaje == 1)
										<label>
											<input type="radio" name="rating" value="5" disabled/>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon">✩</span>
											<span class="icon">✩</span>
											<span class="icon">✩</span>
											<span class="icon">✩</span>
										</label>
										@elseif($opinion->puntaje == 2)
										<label>
											<input type="radio" name="rating" value="5" disabled/>
											<span class="icon" style="color: orange;" >✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon">✩</span>
											<span class="icon">✩</span>
											<span class="icon">✩</span>
										</label>
										@elseif($opinion->puntaje == 3)
										<label>
											<input type="radio" name="rating" value="5" disabled/>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon">✩</span>
											<span class="icon">✩</span>
										</label>
										@elseif($opinion->puntaje == 4)
										<label>
											<input type="radio" name="rating" value="5" disabled/>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon">✩</span>
										</label>								
										@elseif($opinion->puntaje == 5)
										<label>
											<input type="radio" name="rating" value="5" disabled/>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
											<span class="icon" style="color: orange;">✩</span>
										</label>
										@endif
										</div>
							</div>
							
						</div>
						<div style="display: flex;flex-direction: row;align-items: center;padding-left: 10px">
										<div class="img-perfil" style="padding-right: 10px">
										<img class="card-img-top"src="/img/<?=App\Http\Controllers\inicio::img($opinion->usuario)?>"alt="Card image cap"style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">											
										</div>
										<small id="agregadopor" class="form-text text-muted"
											style="margin-top: 0px;padding-right: 10px">Comentado por</small>
										<h7 class="card-title" style="margin-bottom: 0px">{{$opinion->usuario}}</h7>

						</div>
					</div><hr>

					@endforeach
				
				@endif
				</div>
			</div>
			</div>


		</div>


	</div>
</div>

<!-- Mapa y actividades-->
<div class="card" id="mapa">

		 <div id="map" style="border:0;width: 100%;height: 300px;margin:0px" ></div>

	<div class="card-header">

		<h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px"> ACTIVIDAD RECIENTE</h5>
	</div>
	<div class="actividadesrecientes" style="width: 100%; height: 300px; overflow-y: scroll;padding-left: 20px;">
	@foreach ($actividades as $ActividadesItem)	
		<div class="card" style="border: none">

			<div class="actividad" style="padding: 10px;display: flex;flex-direction: row;align-items: center;justify-content: space-between;">

				<div class="img-perfil" style="display: flex;flex-direction: column;">
					<div style="display: flex;flex-direction: row;align-items: center;">
						<img class="card-img-top"
							src="https://pbs.twimg.com/profile_images/1228114737583599616/zqw7-gHc_400x400.jpg"
							alt="Card image cap"
							style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">

						<div style="padding-left: 10px">
							<h7 class="card-title" style="margin-bottom: 0px;font-weight: 700; font-size: 15px;">{{$ActividadesItem->nombreViaje}}</h7>
						</div>
						<div class="actividades">
							<div class="card" style="border: none ">
								<div style="display: flex;flex-direction: row;align-items: center;padding: 10px">

									<small id="agregadopor" class="form-text text-muted"
										style="margin-top: 0px;padding-left: 10px;">{{$ActividadesItem->usuario}}{{$ActividadesItem->actividad}} </small>
								</div>
							</div>
						</div>


					</div>

					<p class="card-text"><small class="text-muted">Publicado {{$ActividadesItem->created_at}} </small></p>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>

	
	  <!-- Modal Clonar Viaje-->
	  <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nuevo Viaje</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
                <form action="{{ route('ClonViaje') }}" method="POST">
					{{ csrf_field() }}
					<input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
						<div class="form-group">
							<label for="exampleInputPassword1">Dale un Nombre</label>
							<input type="text" name="nombreV" class="form-control" id="exampleInputPassword1" placeholder="Nombre del viaje">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Descripcion</label>
							<input type="text" name="descripcion" class="form-control" id="exampleInputPassword1" placeholder="Ingresa una descripcion">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" name="privacidad" for="exampleCheck1">Selecciona para que sea privado</label>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">categoria</label>
							<input type="text" name="categoria" class="form-control" id="exampleInputPassword1" placeholder="#categoria">
						</div>
					

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark" data-toggle="modal" data-target="#invitar">Crear</button>
                </div>
                </form>
				</div>
			</div>
		</div>
	</div>

<!-- Modal Agregar Plan-->
<div class="modal fade addplan" tabindex="-1" role="dialog" aria-labelledby="addplan" aria-hidden="true" id="addplan">

		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Agregar Plan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
                <form action="{{ route('newPlan') }}" method="POST">
					{{ csrf_field() }}
					<input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
						<div class="form-group">
							<label for="exampleInputEmail1">Ingrese un Nombre</label>
							<input type="text" name="nombre" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese un nombre">
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Cuentanos un poco de este lugar</label>
							<textarea name="descripcion" class="form-control" id="exampleFormControlTextarea1" rows="3"
								placeholder="Ingrese una descripcion"></textarea>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Donde queda?</label>
							 <div id="map2" style="border:0;width: 100%;height: 300px;margin:0px" ></div>
							 <input id="lat" name="lat" type="hidden" value="">
							 <input id="lng" name="lng" type="hidden" value="">
							<input type="text" name="ubicacion" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ubicacion">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">¿Algun link para conocer mas de este lugar?</label>
							<input type="text" name="link" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="link aqui">
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark">Agregar</button>
				</div>
				</form>
			</div>
		</div>
	</div>

<!-- Modal Agregar Destino-->
	<div class="modal fade adddestino" tabindex="-1" role="dialog" aria-labelledby="adddestino" aria-hidden="true" id="adddestino">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Agregar Destino</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{ route('newDestino') }}" method="POST">
						{{ csrf_field() }}
						<input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
						<div class="form-group">
							<label for="exampleInputEmail1">Pais</label>
							<input type="text" name="pais" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese un Pais">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Ciudad</label>
							<input type="text" name="ciudad" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese una ciudad">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Sitio Historico o Popular</label>
							<input type="text" name="sitio" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese un sitio a visitar">
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark">Agregar</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal Invitar y Compartir-->
	<div class="modal fade invitar" tabindex="-1" role="dialog" aria-labelledby="invitar" aria-hidden="true" id="invitar">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Invitar a este Itinerario</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<ul class="nav nav-tabs" id="myTab" role="tablist" style="border:none">
						<li class="nav-item" style="border:none">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#colaboradores" role="tab"
								aria-controls="home" aria-selected="true" style="border:none">Colaboradores</a>
						</li>
						<li class="nav-item" style="border:none">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#viajeros" role="tab" aria-controls="profile"
								aria-selected="false" tyle="border:none">Viajeros</a>
						</li>
					</ul>
					<div class="card">
						<div class="card-body" style="border:none;">
							<p class="card-text">Comparte este link con quienes vayas a viajar.Ellos podran cargar nuevas ciudades y
								planes, y ademas podran aprobar las sugerencias de sus otros amigos.</p>
						</div>
						<div class="card-body" style="border:none;    display: flex;flex-direction: row;justify-content: space-between;align-items: center;">
							<form name="f1">
							<div style="width: 100%;">
							<textarea cols="50" rows="5" name="campo1">http://localhost:8000<?=$_SERVER['REQUEST_URI']?>/{{ $viaje->idviaje }}</textarea>
								
							</div>
							<div>

								<!-- The button used to copy the text -->
								<input type="button" value="Copiar" onclick="copia_portapapeles()">
							</div>
							</form>
						</div>
					</div>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="colaboradores" role="tabpanel" aria-labelledby="home-tab">
							<div class="card" style="border:none">
								<div class="card-header" style="font-weight: 800;">
									Ya fueron invitados
								</div>
								<div class="card" style="display: flex;flex-direction: column;justify-content: space-between;align-items: center;border:none; overflow-y: scroll;height: 150px;   padding: 10px;">
									@if($colaboradores->isEmpty())
										<div>Aun no se ha invitado a nadie</div>
									@else
									@foreach($colaboradores as $colab)
									<div class="user-img" style="display: flex;flex-direction: row;justify-content: space-between;width: 100%;align-items: center;">
										<div class="" style="display: flex;flex-direction: row;width: 50%;justify-content: space-between;align-items: center;">
										<div class="img-perfil" style="padding-right: 10px;border-radius: 20px;">
											<img class="card-img-top"src="/img/<?=App\Http\Controllers\inicio::img($colab->nickname)?>"alt="Card image cap"style="border: 1px solid black;border-radius: 20px; width: 40px;height: 40px;">
										</div>
											<div class="user" style="display:flex;flex-direction: column">
												<strong>{{$colab->NombreCompleto}}</strong>
												<small class="form-text text-muted">{{$colab->email}}</small>
											</div>
										</div>

										<div>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
									@endforeach
									@endif
								</div>
								<div>
										<form action="{{ route('newColab') }}" method="POST">
										{{ csrf_field() }}
										<input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
												<div>
													<h5 class="modal-title" id="exampleModalLabel" style=" margin-top: 10px;margin-bottom: 10px;">Quieres invitar a otro colaborador?</h5>
												</div>
												<div>
													<div class="form-group" style="display: flex;">
														<input type="email" name="emailcolab" class="form-control" id="exampleFormControlInput1"placeholder="Ingresa un email para invitar a planear">
														<button type="submit" class="btn btn-dark">Invitar</button>
													</div>
												</div>
										</form>
								</div>

							</div>
						</div>
						<div class="tab-pane fade" id="viajeros" role="tabpanel" aria-labelledby="profile-tab">
							<div class="tab-pane fade show active" id="colaboradores" role="tabpanel" aria-labelledby="home-tab">
								<div class="card" style="border:none">
									<div class="card-header" style="font-weight: 800;">
										Ya fueron invitados
									</div>
									<div class="card" style="display: flex;flex-direction: column;justify-content: space-between;align-items: center;border:none; overflow-y: scroll;    height: 150px;   padding: 10px;">
										@if($viajeros->isEmpty())
											<div>Aun no se ha invitado a nadie</div>
										@else
										@foreach($viajeros as $viajero)
										<div class="user-img" style="display: flex;flex-direction: row;justify-content: space-between;width: 100%;align-items: center;">
											<div class="" style="display: flex;flex-direction: row;width: 50%;justify-content: space-between;align-items: center;">
												<div class="img-perfil" style="padding-right: 10px;border-radius: 20px;">
													<img class="card-img-top"src="/img/<?=App\Http\Controllers\inicio::img($viajero->nickname)?>"alt="Card image cap"style="border: 1px solid black;border-radius: 20px; width: 40px;height: 40px;">
												</div>
												<div class="user" style="display:flex;flex-direction: column">
													<strong>{{$viajero->NombreCompleto}}</strong>
													<small class="form-text text-muted">{{$viajero->email}}</small>
												</div>
											</div>

											<div>
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
										</div>
										@endforeach
										@endif
									</div>
									<div>
											<form action="{{ route('newViajero') }}" method="POST">
												{{ csrf_field() }}
												<input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
												<div>
													<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px;margin-bottom: 10px;">Quieres invitar a otro viajero?</h5>
												</div>
												<div>
													<div class="form-group" style="display: flex;">
														<input type="email" name="emailviajero" class="form-control" id="exampleFormControlInput1"placeholder="Ingresa un email para invitar a planear">
														<button type="submit" class="btn btn-dark">Invitar</button>
													</div>
												</div>
											</form>
									</div>


								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark">Guardar</button>
				</div>
			</div>
		</div>
	</div>

<!-- Modal Agregar Opinion -->
<div class="modal fade addopinion" tabindex="-1" role="dialog" aria-labelledby="addopinion" aria-hidden="true" id="addopinion">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Que tal te parecio el viaje ?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
                <form action="{{ route('newOpinion') }}" method="POST">
					{{ csrf_field() }}
					<input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Calificar</label>
							<div class="star">
							<label>
									<input type="radio" name="rating" value="1" />
									<span class="icon">✩</span>
								</label>
								<label>
									<input type="radio" name="rating" value="2" />
									<span class="icon">✩</span>
									<span class="icon">✩</span>
								</label>
								<label>
									<input type="radio" name="rating" value="3" />
									<span class="icon">✩</span>
									<span class="icon">✩</span>
									<span class="icon">✩</span>   
								</label>
								<label>
									<input type="radio" name="rating" value="4" />
									<span class="icon">✩</span>
									<span class="icon">✩</span>
									<span class="icon">✩</span>
									<span class="icon">✩</span>
								</label>
								<label>
									<input type="radio" name="rating" value="5" />
									<span class="icon">✩</span>
									<span class="icon">✩</span>
									<span class="icon">✩</span>
									<span class="icon">✩</span>
									<span class="icon">✩</span>
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Comentario (Opcional)</label>
							<textarea name="coment" class="form-control" id="exampleFormControlTextarea1" rows="3"
								placeholder="que tal fue?"></textarea>
						</div>
						


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-dark">Opinar</button>
				</div>
				</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal Finalizar Viaje -->
<div class="modal fade finalizar" tabindex="-1" role="dialog" aria-labelledby="finalizar" aria-hidden="true" id="finalizar">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 450px">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Finalizar Viajee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form action="{{ route('finViaje') }}" method="POST">
                    {{ csrf_field() }}
                    <input id="prodId" name="id" type="hidden" value="{{ $viaje->idviaje }}">
                    <h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px"> Seguro que quiere dar por terminado el viaje?</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Confirmar</button>
                </div>
                </form>
            </div>
        </div>
    </div>

	<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyFwKSbrJP4nb2pajkgR5BgRKu2yL1CEA&callback=initMap&libraries=&v=weekly"
      defer
    ></script>
	<script>

var marker;

function initMap() {

var variable = <?php echo json_encode($planes); ?>;
var a = -34.3375015;
var b = -56.7136116;

if(variable.length > 1){
	for (var i = 0; i < variable.length; i++) {
	var plan1 = variable[i];
	if(plan1.aprovado){
	
	a = parseInt(plan1.lat);
	b = parseInt(plan1.lng);
	break;
	}
}

}
var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 4,
  center: {lat: a, lng: b}

});
var map2 = new google.maps.Map(document.getElementById('map2'), {
  zoom: 4,
  center: {lat: -34.3375015, lng: -56.7136116 }

});

google.maps.event.addListener(map2, 'click', function(event) {
        addListing(event.latLng,map2);
      });


for (var i = 0; i < variable.length; i++) {
    var plan = variable[i];
	if(plan.aprovado){
    var latLng = new google.maps.LatLng(plan.lat,plan.lng);
    var marker = new google.maps.Marker({
      position: latLng,
      map: map
    });
	}
  }


}
var i=1;
function addListing(location,map2) {
  var addMarker;
  var iMax=1;

  if(i<=iMax) {
      addMarker = new google.maps.Marker({
      draggable:false,
      position: location,
      map: map2
  });

  google.maps.event.addListener(addMarker, 'dblclick', function() {
    addMarker.setMap(null);
    i=1;
	$("#lat").val("");
	$("#lng").val("");
  });

  i++;
	$("#lat").val(addMarker.getPosition().lat());
	$("#lng").val(addMarker.getPosition().lng());

  } else {
      console.log('you can only post' , i-1, 'markers');
      }



}


  </script>


@endsection