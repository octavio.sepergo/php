@extends('Plantilla/plantilla')

@section('contenido')
<div class="conteiner" id="contenido">

	<div class="card" id="viajes" style="height: 700px;padding: 20px;">
		<div class="card text-center" style="border: none;">
			<div class="card-body" style="display: flex;justify-content: space-between;">
				<h5 class="card-title" style="   font-weight: 700;text-align: start;">Itinerarios de Viajes</h5>
				<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal" >NUEVO</button>
			</div>

		<hr>
			@if($viajes->isEmpty())
				<li>No se encontro ningun Viaje</li>
				@else	
				<div class="row">
				@foreach($viajes as $viajesItem)

					<div class="col-sm-6">
						<div class="card">
						<div class="card-body">
							<h5 class="card-title">{{ $viajesItem->nombre }}</h5>
							<p class="card-text">{{ $viajesItem->descripcion }}</p>

							
							<a href="{{ route('viaje2', ['id' => $viajesItem->idviaje]) }}">
							<button type="submit" class="btn btn-dark">Ver mas</button>
							</a>

						</div>
						</div>
					</div>

				@endforeach
				</div>

			@endif	
		<hr>

		</div>
	</div>

<div class="card-header">

<h5 class="card-title" style="margin-bottom: 0px;padding-right: 20px"> ACTIVIDAD RECIENTE</h5>

<div class="actividadesrecientes" style="width: 100%; height: 300px; overflow-y: scroll;padding-left: 20px;">
@foreach ($actividades as $ActividadesItem)	
<a href="{{ route('viaje2', ['id' => $ActividadesItem->idViaje]) }}">
<div class="card" style="border: none">

	<div class="actividad" style="padding: 10px;display: flex;flex-direction: row;align-items: center;justify-content: space-between;">

		<div class="img-perfil" style="display: flex;flex-direction: column;">
			<div style="display: flex;flex-direction: row;align-items: center;">
				<img class="card-img-top"
					src="https://pbs.twimg.com/profile_images/1228114737583599616/zqw7-gHc_400x400.jpg"
					alt="Card image cap"
					style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;">

				<div style="padding-left: 10px">
					<h7 class="card-title" style="margin-bottom: 0px;font-weight: 700; font-size: 15px;">{{$ActividadesItem->nombreViaje}}</h7>
				</div>
				<div class="actividades">
					<div class="card" style="border: none ">
						<div style="display: flex;flex-direction: row;align-items: center;padding: 10px">

							<small id="agregadopor" class="form-text text-muted"
								style="margin-top: 0px;padding-left: 10px;">{{$ActividadesItem->usuario}}{{$ActividadesItem->actividad}} </small>
						</div>
					</div>
				</div>


			</div>

			<p class="card-text"><small class="text-muted">Publicado {{$ActividadesItem->created_at}} </small></p>
		</div>
		
	</div>
</div>
</a>
@endforeach
</div>
@endsection

