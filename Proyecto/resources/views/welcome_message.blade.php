<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<title>TridoR</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script language="javascript" type="text/javascript">
		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').trigger('focus')
		});

	</script>
</head>
<style>
	.modal {
		text-align: center;
		padding: 0 !important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 80%;
		vertical-align: middle;
		margin-right: -3px;
		/* Adjusts for spacing */
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}

	#contenido {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
	}

	#viajes {
		width: 80%;
		margin-left: 20px;
		border: none;
	}

	#mapa {
		width: 35%;
		height: 650px;
		margin: 20px;
		-webkit-box-shadow: 8px 9px 10px -8px rgba(0, 0, 0, 0.75);
		-moz-box-shadow: 8px 9px 10px -8px rgba(0, 0, 0, 0.75);
		box-shadow: 8px 9px 10px -8px rgba(0, 0, 0, 0.75);
	}

	@media (max-width: 992px) {
		#contenido {
			flex-direction: column;
		}

		#viajes {
			width: 100%;
			margin: 0px;
		}

		#mapa {
			width: 100%;
			margin: 0px;
			padding: 10px;

		}
	}

</style>

<div class="card">
	<img src="https://2.bp.blogspot.com/--3LOaU0HSNc/T3yLIRpWNTI/AAAAAAAALVo/o9XLrdsw4nY/s1600/lugares+hd+%282%29.jpg"
		class="img-fluid" alt="Responsive image">
	<div class="carousel-caption d-none d-md-block" style="padding-bottom: 20%">
		<h1 style="    font-size: 70px;
    font-weight: 800;
    color: floralwhite;
    -webkit-text-stroke-width: 2px;
    -webkit-text-stroke-color: black;">Bienvenidos a TripDo</h1>
		<h4 style="font-size: 30px;
    font-weight: 800;
    color: ghostwhite;
    -webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: grey;">Preparado para la aventura? <p>{{Session::get('nick')}}</p> </h4>

			<a  class="btn btn-dark" style="border-radius: 50%;" href="{{ route('principal') }}">Ingresar al sitio</a>

	</div>
</div>
