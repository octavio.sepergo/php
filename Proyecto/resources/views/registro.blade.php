@extends('welcome')

  
@section ('content')
  <form action="{{ route('registro') }}" method="POST">
  {{ csrf_field() }}
  <h2><em>Formulario de Registro</em></h2>  
     
      <label for="Nick">Nick <span><em>(requerido)</em></span></label>
      <input type="text" name="Nick" class="form-input" required/>   
      <br>
      <label for="nombre">nombre <span><em>(requerido)</em></span></label>
      <input type="text" name="nombre" class="form-input" required/>   
      <br>
      <label for="telefono">telefono<span><em>(requerido)</em></span></label>
      <input type="text" name="telefono" class="form-input" required/>         
      <br>
      <label for="email">Email <span><em>(requerido)</em></span></label>
      <input type="email" name="email" class="form-input" />
      <br>
      <label for="pass">pass <span><em>(requerido)</em></span></label>
      <input type="text" name="pass" class="form-input" />
      <br>
     <input class="form-btn" name="submit" type="submit" value="Suscribirse" />
    </p>
  </form>
@endsection