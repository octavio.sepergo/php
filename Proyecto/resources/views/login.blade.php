<form action="{{ route('login') }}" method="POST">
{{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Nickname</label>
    <input type="text" class="form-control" name="Nick" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nick">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="pass" id="exampleInputPassword1" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Ingresar</button>
</form>