
	<!-- Modal Login-->
	<div class="modal fade login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true" id="login">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title">Ingresar Sesion</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputEmail1">Nickname</label>
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su nick">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Ingrese su Password">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" for="exampleCheck1">Mantenerme logeado</label>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-dark">Ingresar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
