	<!-- Modal Crear Viaje-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nuevo Viaje</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputPassword1">Dale un Nombre</label>
							<input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre del viaje">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Descripcion</label>
							<input type="text" class="form-control" id="exampleInputPassword1" placeholder="Ingresa una descripcion">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" for="exampleCheck1">Selecciona para que sea privado</label>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#invitar">Crear</button>

				</div>
			</div>
		</div>
	</div>
