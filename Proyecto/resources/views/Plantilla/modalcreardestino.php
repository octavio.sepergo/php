	<!-- Modal Agregar Destino-->
	<div class="modal fade adddestino" tabindex="-1" role="dialog" aria-labelledby="adddestino" aria-hidden="true"
		id="adddestino">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Agregar Destino</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<iframe src="https://maps.google.com/maps?q=manhatan&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
							style="border:0;width: 100%;height: 300px;margin:0px" allowfullscreen></iframe>
						<div class="form-group">
							<label for="exampleInputEmail1">Ciudad</label>
							<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese una ciudad">
						</div>
						<select class="mdb-select md-form">
							<option value="" disabled selected>Tipo de sitio a visitar</option>
							<option value="1">Sitio Historico</option>
							<option value="2">Sitio Popular</option>
						</select>
						<div class="form-group">
							<label for="exampleInputEmail1">Descripcion</label>
							<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Mas informacion sobre el sitio">
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-dark">Agregar</button>
				</div>
				</form>
			</div>
		</div>
	</div>