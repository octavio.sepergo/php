	<!-- Modal Registro-->
	<div class="modal fade registro" tabindex="-1" role="dialog" aria-labelledby="registro" aria-hidden="true"
		id="registro">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Registrarme a TripDo</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputEmail1">Nickname</label>
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su nick">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su email">
							<small id="emailHelp" class="form-text text-muted">Nunca compartiremos su email con
								nadie.</small>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Biografia</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
								placeholder="Ingrese una descripcion para conocer mas de usted"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleFormControlFile1">Seleccione una imagen de perfil</label>
							<input type="file" class="form-control-file" id="exampleFormControlFile1">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" class="form-control" id="exampleInputPassword1"
								placeholder="Ingrese su Password">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" for="exampleCheck1">Mantenerme logeado</label>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-dark">Registrarme</button>
				</div>
				</form>
			</div>
		</div>
	</div>
