<!DOCTYPE html>
<html lang="es">

<head>

  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Your Website Title" />
  <meta property="og:description"   content="Your description" />
  <meta property="og:image"         content="https://2.bp.blogspot.com/--3LOaU0HSNc/T3yLIRpWNTI/AAAAAAAALVo/o9XLrdsw4nY/s1600/lugares+hd+%282%29.jpg" />

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>TridoR</title>
	<link rel="shortcut icon" href="https://2.bp.blogspot.com/--3LOaU0HSNc/T3yLIRpWNTI/AAAAAAAALVo/o9XLrdsw4nY/s1600/lugares+hd+%282%29.jpg">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script language="javascript" type="text/javascript">
		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').trigger('focus')
		});

	</script>

</head>
<style>
	 /* Always set the map height explicitly to define the size of the div
       * element that contains the map. /
      #map {
        height: 100%;
      }

      / Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
	.star {
    display: flex;
    position: relative;
    height: 40px;
    line-height: 50px;
    font-size: 40px;
}

.star label {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  cursor: pointer;
}

.star label:last-child {
  position: static;
}

.star label:nth-child(1) {
  z-index: 5;
}

.star label:nth-child(2) {
  z-index: 4;
}

.star label:nth-child(3) {
  z-index: 3;
}

.star label:nth-child(4) {
  z-index: 2;
}

.star label:nth-child(5) {
  z-index: 1;
}

.star label input {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
}

.star label .icon {
  float: left;
  color: transparent;
}

.star label:last-child .icon {
  color: #000;
}

.star:not(:hover) label input:checked ~ .icon,
.star:hover label:hover input ~ .icon {
    color: orange;
}

.star label input:focus:not(:checked) ~ .icon:last-child {
  color: #000;
  text-shadow: 0 0 5px #09f;
}



.star2 {
    display: flex;
    position: relative;
    height: 10px;
    line-height: 50px;
    font-size: 20px;
}

.star2 label {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  cursor: pointer;
}

.star2 label:last-child {
  position: static;
}

.star2 label:nth-child(1) {
  z-index: 5;
}

.star2 label:nth-child(2) {
  z-index: 4;
}

.star2 label:nth-child(3) {
  z-index: 3;
}

.star2 label:nth-child(4) {
  z-index: 2;
}

.star2 label:nth-child(5) {
  z-index: 1;
}

.star2 label input {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
}

.star2 label .icon {
  float: left;
  color: transparent;
}

.star2 label:last-child .icon {
  color: #000;
}

.star2 label input:focus:not(:checked) ~ .icon:last-child {
  color: #000;
  text-shadow: 0 0 5px #09f;
}
	.modal {
		text-align: center;
		padding: 0 !important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 80%;
		vertical-align: middle;
		margin-right: -3px;
		/* Adjusts for spacing */
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}

	#contenido {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
	}

	#viajes {
		width: 100%;
		margin-left: 20px;
		border: none;
	}

	#mapa {
		width: 35%;
		height: 650px;
		margin: 20px;
		-webkit-box-shadow: 8px 9px 10px -8px rgba(0, 0, 0, 0.75);
		-moz-box-shadow: 8px 9px 10px -8px rgba(0, 0, 0, 0.75);
		box-shadow: 8px 9px 10px -8px rgba(0, 0, 0, 0.75);
	}

	@media (max-width: 992px) {
		#contenido {
			flex-direction: column;
		}

		#viajes {
			width: 100%;
			margin: 0px;
		}

		#mapa {
			width: 100%;
			margin: 0px;
			padding: 10px;

		}
	}

</style>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="{{ route('principal') }}">TripDo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
			aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown" style="justify-content: space-between;">
			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					@if(session()->get('nick') != null)
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">
						Viajes
					</a>
					
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="{{ route('MisViajes') }}">Mis Viajes</a>
						<a class="dropdown-item" href="" data-toggle="modal" data-target="#exampleModal" >Nuevo Viaje</a>
					</div>
					@endif
				</li>

            </ul>
			<form class="form-inline" action="{{ route('buscarViaje') }}" method="POST">
				{{ csrf_field() }}
				<span class="input-group-text">#</span>
				<input class="form-control mr-sm-2" type="search" name="viaje" placeholder="#categorias" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
			</form>
        
			<ul class="navbar-nav" style="    width: 20%;
    display: flex;
    flex-direction: row;
    justify-content: space-around;">
        @if(Session::get('nick') =="")

				<li class="nav-item">
					<a href="#registro" data-toggle="modal" data-target="#registro" style="color: white">Registro</a>
				</li>
				<li class="nav-item">
					<a href="#login" data-toggle="modal" data-target="#login" style="color: white">Login</a>
				</li>

      
        @else

				<li class="nav-item">
					<a href="#" style="color: white">{{Session::get('nick')}}</a>
                </li>
				<li class="nav-item">
                <div class="card" style="border-radius: 20px;">

                <img class="card-img-top"
                            src="/img/<?=App\Http\Controllers\inicio::img(session()->get('nick'))?>"
                            alt="Card image cap"
                            style="border: 1px solid black;border-radius: 20px; width: 30px;height: 30px;"></div>
                </li>          
				<li class="nav-item">
					<a href="{{ route('logout') }}" style="color: white">Logout</a>
				</li>

        @endif        
			</ul>
		</div>

    </nav>
    
    @yield('contenido')

    
<div class="modal fade login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true" id="login">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title">Ingresar Sesion</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{ route('login') }}" method="POST">
                    {{ csrf_field() }}
						<div class="form-group">
							<label for="exampleInputEmail1">Nickname</label>
							<input type="text" class="form-control" id="Nick" name="Nick" aria-describedby="emailHelp"
								placeholder="Ingrese su nick">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="Ingrese su Password">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" for="exampleCheck1">Mantenerme logeado</label>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark">Ingresar</button>
				</div>
				</form>
			</div>
		</div>
	</div>

    <!-- Modal Registro-->
	<div class="modal fade registro" tabindex="-1" role="dialog" aria-labelledby="registro" aria-hidden="true"
		id="registro">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Registrarme a TripDo</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{ route('registro') }}" method="POST" enctype= multipart/form-data>
                    {{ csrf_field() }}
						<div class="form-group">
							<label for="exampleInputEmail1">Nickname</label>
							<input type="text" class="form-control" name="Nick" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su nick">
                        </div>
                        <div class="form-group">
							<label for="exampleInputEmail1">Nombre</label>
							<input type="text" class="form-control" name="nombre" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su nombre">
                        </div>
                        <div class="form-group">
							<label for="exampleInputEmail1">Telefono</label>
							<input type="text" class="form-control" name="telefono" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su Telefono">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su email">
							<small id="emailHelp" class="form-text text-muted">Nunca compartiremos su email con
								nadie.</small>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Biografia</label>
							<textarea class="form-control" name="biografia" id="exampleFormControlTextarea1" rows="3"
								placeholder="Ingrese una descripcion para conocer mas de usted"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleFormControlFile1">Seleccione una imagen de perfil</label>
							<input type="file" name="img" class="form-control-file" id="exampleFormControlFile1">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" name="pass" class="form-control" id="exampleInputPassword1"
								placeholder="Ingrese su Password">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" for="exampleCheck1">Mantenerme logeado</label>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark">Registrarme</button>
				</div>
				</form>
			</div>
		</div>
	</div>

   <!-- Modal Crear Viaje-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 450px">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nuevo Viaje</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
                <form action="{{ route('newViaje') }}" method="POST">
                    {{ csrf_field() }}
						<div class="form-group">
							<label for="exampleInputPassword1">Dale un Nombre</label>
							<input type="text" name="nombreV" class="form-control" id="exampleInputPassword1" placeholder="Nombre del viaje">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Descripcion</label>
							<input type="text" name="descripcion" class="form-control" id="exampleInputPassword1" placeholder="Ingresa una descripcion">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" name="privacidad" for="exampleCheck1">Selecciona para que sea privado</label>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">categoria</label>
							<input type="text" name="categoria" class="form-control" id="exampleInputPassword1" placeholder="#categoria">
						</div>
					

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark" data-toggle="modal" data-target="#invitar">Crear</button>
                </div>
                </form>
				</div>
			</div>
		</div>
	</div>
     



    <footer class="page-footer fixed-bottom font-small bg-dark">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="color: whitesmoke">© 2020 Copyright
  </div>
  <!-- Copyright -->

</footer>


    <script src="https://kit.fontawesome.com/d2e9cc5742.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


</body>
</html>