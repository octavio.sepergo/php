<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Formulario de Registro</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>

<div class="modal-body">
					<form action="{{ route('registroInvitado') }}" method="POST" enctype= multipart/form-data>
                    {{ csrf_field() }}
						<input id="prodId" name="id" type="hidden" value="{{ $id }}">
						<input id="prodId2" name="tipo" type="hidden" value="{{ $tipo }}">          
						<div class="form-group">
							<label for="exampleInputEmail1">Nickname</label>
							<input type="text" class="form-control" name="Nick" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su nick">
                        </div>
                        <div class="form-group">
							<label for="exampleInputEmail1">Nombre</label>
							<input type="text" class="form-control" name="nombre" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su nombre">
                        </div>
                        <div class="form-group">
							<label for="exampleInputEmail1">Telefono</label>
							<input type="text" class="form-control" name="telefono" id="exampleInputEmail1" aria-describedby="emailHelp"
								placeholder="Ingrese su Telefono">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
              readonly="readonly" value="{{ $email }}">
							<small id="emailHelp" class="form-text text-muted">Nunca compartiremos su email con
								nadie.</small>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Biografia</label>
							<textarea class="form-control" name="biografia" id="exampleFormControlTextarea1" rows="3"
								placeholder="Ingrese una descripcion para conocer mas de usted"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleFormControlFile1">Seleccione una imagen de perfil</label>
							<input type="file" name="img" class="form-control-file" id="exampleFormControlFile1">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" name="pass" class="form-control" id="exampleInputPassword1"
								placeholder="Ingrese su Password">
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="exampleCheck1">
							<label class="form-check-label" for="exampleCheck1">Mantenerme logeado</label>
						</div>
				</div>
        <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-dark">Registrarme</button>
				</div>
				</form>
</body>
</html>